import React from 'react';

import Routes from './routes';

MapboxGL.setAccessToken('pk.eyJ1IjoiaGVucmlxdWVib3RlZ2EiLCJhIjoiY2prd3g3Y21iMDJuODN2bDdkNXdpdHhhcCJ9.ZWGFlH_kWckW-VkhrSyu2Q');

const App = () => <Routes />;

export default App;