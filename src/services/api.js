import axios from 'axios';
import { Platform } from 'react-native';

const localhost = (Platform.OS === 'android') ? '10.0.3.2' : 'localhost';

const api = axios.create({
    baseURL: 'http://' + localhost + ':3333/',
});

api.interceptors.request.use(async (config) => {
    try {
        const token = await AsyncStorage.getItem('@AirBnbApp:token');

        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }

        return config;
    } catch (err) {
        alert(err);
    }
});

export default api;